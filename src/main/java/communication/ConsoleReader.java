package communication;

import calculator.MainCalculator;
import converter.InfixConverter;
import normalizer.ExpressionNormalizer;
import util.support.MathContextKeeper;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.util.Scanner;

public class ConsoleReader {
    private final  char DECIMAL_SEPARATOR = DecimalFormatSymbols.getInstance().getDecimalSeparator();
    private boolean isReadingFinished;
    private MathContextKeeper context;
    private InfixConverter infixConverter;
    private ExpressionNormalizer normalizer;
    private MainCalculator calculator;

    public ConsoleReader() {
        this.isReadingFinished = false;
        this.context = new MathContextKeeper();
        this.infixConverter = new InfixConverter(context);
        this.normalizer = new ExpressionNormalizer(context);
        this.calculator = new MainCalculator(context);
    }

    public void communicate() {
        String info = creatingInformationMessage();
        System.out.println(info);
        try (Scanner scanner = new Scanner(System.in)) {
            while (!isReadingFinished) {
                try {
                    String userInput = scanner.nextLine().toLowerCase().trim();
                    switch (userInput) {
                        case "end":
                            isReadingFinished = true;
                            System.out.println("Closing program...");
                            break;
                        case "inf":
                            System.out.println(creatingInformationMessage());
                            break;
                        default:
                            calculateExpression(userInput);
                    }
                } catch (ArithmeticException ae) {
                    System.out.println("An arithmetic error occurred while evaluating the expression: " + ae.getMessage());
                } catch (RuntimeException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    private String creatingInformationMessage() {
        return  String.format("The program accepts an expression of the form: 4.2 + 2 * 3 / 3 - 6.1\n" +
                "Expression numbers can be fractional. Use a separator: %s\n" +
                "Forbidden to duplicate operators in an expression\n" +
                "Enter end to finish or inf to print information", DECIMAL_SEPARATOR);
    }

    private void calculateExpression(String expression) {
        String normalizeExp = normalizer.normalize(expression);
        System.out.println("Expression using normalization: " + normalizeExp);
        String postfix = infixConverter.convert(normalizeExp);
        System.out.println("Expression converted to postfix: " + postfix);
        BigDecimal result = calculator.calculate(postfix);
        System.out.println("The result of evaluating an expression: " + postfix);
        System.out.println(result);
    }
}
