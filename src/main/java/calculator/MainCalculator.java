package calculator;

import util.support.MathContextKeeper;

import java.math.BigDecimal;
import java.util.Scanner;
import java.util.Stack;

public class MainCalculator {
    private final MathContextKeeper context;

    public MainCalculator(MathContextKeeper context) {
        this.context = context;
    }

    public BigDecimal calculate(String postfix) {
        if (postfix == null || postfix.isBlank()) {
            throw new IllegalArgumentException("postfix cannot be null or empty");
        }
        Stack<BigDecimal> stack = new Stack<>();
        PostfixTokenAnalyzer handler = new PostfixTokenAnalyzer(context);
        try (Scanner scanner = new Scanner(postfix)) {
            scanner.useDelimiter(" ");
            while (scanner.hasNext()) {
                handler.analyze(scanner.next(),stack);
            }
            return stack.pop();
        }
    }
}
