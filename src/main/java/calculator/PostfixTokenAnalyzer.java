package calculator;

import util.support.MathContextKeeper;
import util.support.TokenUtil;
import util.token.Associativity;
import util.token.OperandToken;
import util.token.OperatorToken;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static util.support.TokenUtil.getOperator;
import static util.support.TokenUtil.isOperator;

public class PostfixTokenAnalyzer {
    private MathContextKeeper context;
    private Optional<CharSequence> value = Optional.empty();
    private final Map<Predicate<CharSequence>,
            Function<Optional<CharSequence>, Consumer<Stack<BigDecimal>>>> tokenHandler;

    public PostfixTokenAnalyzer(MathContextKeeper context) {
        this.context = context;
        this.tokenHandler = new LinkedHashMap<>();
        tokenHandler.put(TokenUtil::isOperand,value -> stack -> stack.push(OperandToken.create(value.get()).getValue()));
        tokenHandler.put(token->isOperator(token,context),operatorHandler);
    }

    private final Function<Optional<CharSequence>, Consumer<Stack<BigDecimal>>> operatorHandler = value -> stack -> {
        OperatorToken operator = getOperator(value.get(),context);
        BigDecimal arg1 = BigDecimal.ZERO;
        BigDecimal arg2 = BigDecimal.ZERO;
        Associativity associativity = operator.getAssociativity();
        switch (associativity) {
            case RIGHT:
                arg1 = stack.pop();
                arg2 = stack.pop();
                break;
            case LEFT:
                arg2 = stack.pop();
                arg1 = stack.pop();
                break;
            default:
                break;
        }
        BigDecimal result = operator.getOperation().apply(new BigDecimal[]{arg1,arg2});
        stack.push(result);
    };


    public void analyze(CharSequence token,Stack<BigDecimal> stack) {
        tokenHandler.entrySet().stream().filter(e-> {
                    if (e.getKey().test(token)) {
                        value = Optional.of(token);
                        return true;
                    }
                    return false;
                }).map(e-> {
                    e.getValue().apply(value).accept(stack);
                    value = Optional.empty();
                    return true;
                }).findAny().orElseThrow(()->new ArithmeticException(
                String.format("offending postfix postfix '%s'", token)
        ));
    }

}
