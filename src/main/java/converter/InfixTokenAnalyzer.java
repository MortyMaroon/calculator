package converter;

import util.support.MathContextKeeper;
import util.token.OperatorToken;
import util.support.TokenUtil;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static util.support.TokenUtil.*;

public class InfixTokenAnalyzer {
    private MathContextKeeper context;
    private Optional<CharSequence> value = Optional.empty();
    private final Map<Predicate<CharSequence>,
            Function<Optional<CharSequence>, BiConsumer<Stack<CharSequence>, Queue<CharSequence>>>> tokenHandler = new LinkedHashMap<>();

    public InfixTokenAnalyzer(final MathContextKeeper context) {
        this.context = context;
        tokenHandler.put(TokenUtil::isOperand,value->(stack,queue)->queue.offer(value.get()));
        tokenHandler.put(TokenUtil::isOpeningParentheses,value->(stack,queue)->stack.push(value.get()));
        tokenHandler.put(TokenUtil::isClosingParentheses,closingParenthesesHandler);
        tokenHandler.put(token->isPrefixOperator(token,context),prefixHandler);
        tokenHandler.put(token->isPostfixOperator(token,context),postfixHandler);
    }

    private final Function<Optional<CharSequence>,BiConsumer<Stack<CharSequence>,Queue<CharSequence>>> closingParenthesesHandler =
            value->(stack,queue)-> {
                while (!stack.isEmpty() && !isOpeningParentheses(stack.peek())) {
                    if (isOperator(stack.peek(),context)) {
                        queue.offer(stack.pop());
                    }
                }
                if (stack.isEmpty() || !isOpeningParentheses(stack.peek())) {
                    throw new IllegalArgumentException("Malformed expression");
                } else {
                    stack.pop();
                }
            };

    private final Function<Optional<CharSequence>,BiConsumer<Stack<CharSequence>,Queue<CharSequence>>> postfixHandler =
            value->(stack,queue)-> {
                OperatorToken operatorToken = getOperator(value.get(), context);
                while (!stack.isEmpty() && isOperator(stack.peek(), context) && getOperator(stack.peek(), context)
                        .getPriority().compareTo(operatorToken.getPriority()) > 0) {
                    queue.offer(stack.pop());
                }
                stack.push(value.get());
            };

    private final Function<Optional<CharSequence>,BiConsumer<Stack<CharSequence>,Queue<CharSequence>>> prefixHandler =
            value->(stack,queue)-> {
                OperatorToken operatorToken = getOperator(value.get(), context);
                while (!stack.isEmpty() && isOperator(stack.peek(), context) && getOperator(stack.peek(), context)
                        .getPriority().compareTo(operatorToken.getPriority()) >= 0) {
                    queue.offer(stack.pop());
                }
                stack.push(value.get());
            };

    public void analyze(CharSequence token, Stack<CharSequence> stack, Queue<CharSequence> queue) {
        tokenHandler.entrySet().stream().filter(e-> {
                if (e.getKey().test(token)) {
                    value = Optional.of(token);
                    return true;
                }
                return false;
            }).map(e->{
                if (value.isEmpty()) {
                    throw new UnsupportedOperationException("Cannot handle empty token");
                }
                e.getValue().apply(value).accept(stack,queue);
                value = Optional.empty();
                return true;
            }).findAny().orElseThrow(()-> new IllegalArgumentException(
            String.format("No matching token handler for accepting and handling value '%s'%n"
                        + "The error cause is an unknown operand, operator or missing space.%n"
                        + "Please fix the input infix expression.", token)
        ));
    }

}
