package converter;

import util.support.MathContextKeeper;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class InfixConverter {
    private final MathContextKeeper context;

    public InfixConverter(MathContextKeeper context) {
        this.context = context;
    }

    public String convert(final CharSequence expression) {
        Queue<CharSequence> queue = new LinkedList<>();
        Stack<CharSequence> stack = new Stack<>();
        InfixTokenAnalyzer analyzer = new InfixTokenAnalyzer(context);
        try (Scanner scanner = new Scanner(expression.toString())) {
            scanner.useDelimiter(" ");
            while (scanner.hasNext()) {
                analyzer.analyze(scanner.next(),stack,queue);
            }
            while (!stack.isEmpty()) {
                queue.offer(stack.pop());
            }
        }
        return String.join(" ", queue);
    }
}
