package util.token;

import java.math.BigDecimal;
import java.util.function.Function;

import static util.token.Associativity.LEFT;

public class OperatorToken {
    private CharSequence symbol;
    private Associativity associativity;
    private Priority priority;
    private Function<BigDecimal[],BigDecimal> operation;

    public OperatorToken(CharSequence symbol, Priority priority, Associativity associativity, Function<BigDecimal[], BigDecimal> operation) {
        this.symbol = symbol;
        this.associativity = associativity;
        this.priority = priority;
        this.operation = operation;
    }

    public static OperatorToken create(CharSequence symbol, Priority priority, Function<BigDecimal[], BigDecimal> operation) {
        return new OperatorToken(symbol, priority, LEFT, operation);
    }

    public static OperatorToken create(CharSequence symbol, Priority priority, Associativity associativity, Function<BigDecimal[], BigDecimal> operation) {
        return new OperatorToken(symbol, priority,associativity, operation);
    }

    public CharSequence getSymbol() {
        return symbol;
    }

    public Associativity getAssociativity() {
        return associativity;
    }

    public Priority getPriority() {
        return priority;
    }

    public Function<BigDecimal[], BigDecimal> getOperation() {
        return operation;
    }
}