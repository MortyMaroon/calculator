package util.token;

import java.math.BigDecimal;
import java.util.Objects;

public class OperandToken {
    private BigDecimal value;

    private OperandToken(final CharSequence value) {
        this.value = new BigDecimal(Objects.requireNonNull(value,"operand cannot be null").toString());
    }

    public static OperandToken create(CharSequence value) {
        try {
            return new OperandToken(Double.valueOf(value.toString().replace(",",".")).toString());
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public BigDecimal getValue() {
        return value;
    }
}
