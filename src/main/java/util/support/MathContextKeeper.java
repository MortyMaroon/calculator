package util.support;

import util.token.OperatorToken;

import static util.token.Priority.*;
import static util.token.Associativity.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

public class MathContextKeeper {
    public static final RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_EVEN;
    public static final int DEFAULT_PRECISION = 7;
    public static final CharSequence OPENING_PARENTHESES = "(";
    public static final CharSequence CLOSING_PARENTHESES = ")";
    private final MathContext mathContext;
    private final List<OperatorToken> defaultOperators;

    public MathContextKeeper() {
        this.mathContext = new MathContext(DEFAULT_PRECISION,DEFAULT_ROUNDING_MODE);
        defaultOperators = List.of(
                OperatorToken.create("^", HIGH,RIGHT,(arr) -> round(arr[0].pow(arr[1].intValue()))),
                OperatorToken.create("*", MIDDLE, (arr) -> round(arr[0].multiply(arr[1]))),
                OperatorToken.create("×", MIDDLE, (arr) -> round(arr[0].multiply(arr[1]))),
                OperatorToken.create("/", MIDDLE, (arr) -> round(arr[0].divide(arr[1], getPrecision(), getRoundingMode()))),
                OperatorToken.create("÷", MIDDLE, (arr) -> round(arr[0].divide(arr[1], getPrecision(), getRoundingMode()))),
                OperatorToken.create("+", LOW,(arr) -> round(arr[0].add(arr[1]))),
                OperatorToken.create("-", LOW,(arr) -> round(arr[0].subtract(arr[1]))));
    }

    public BigDecimal round(BigDecimal value) {
        if (value == null) {
            throw new IllegalArgumentException("cannot round null or NaN value");
        }
        return value.setScale(getPrecision(),getRoundingMode());
    }

    public RoundingMode getRoundingMode() {
        return mathContext.getRoundingMode();
    }

    public int getPrecision() {
        return mathContext.getPrecision();
    }

    public List<OperatorToken> getOperators() {
        return defaultOperators;
    }
}