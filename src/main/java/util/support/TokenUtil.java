package util.support;

import util.token.Associativity;
import util.token.OperatorToken;

import java.text.DecimalFormatSymbols;
import java.util.Objects;

import static util.support.MathContextKeeper.CLOSING_PARENTHESES;
import static util.support.MathContextKeeper.OPENING_PARENTHESES;

public class TokenUtil {

    public static OperatorToken getOperator(CharSequence token, MathContextKeeper context) {
        return context.getOperators().stream().filter(t->t.getSymbol().equals(token)).findAny().orElseThrow();
    }

    public static boolean isOpeningParentheses(CharSequence token) {
        return Objects.requireNonNullElse(token, "").equals(OPENING_PARENTHESES);
    }

    public static boolean isClosingParentheses(CharSequence token) {
        return Objects.requireNonNullElse(token, "").equals(CLOSING_PARENTHESES);
    }

    public static boolean isPostfixOperator(CharSequence token,MathContextKeeper context) {
        return context.getOperators().stream().anyMatch(t->t.getSymbol().equals(token) && t.getAssociativity() == Associativity.RIGHT);
    }

    public static boolean isPrefixOperator(CharSequence token,MathContextKeeper context) {
        return context.getOperators().stream().anyMatch(t->t.getSymbol().equals(token) && t.getAssociativity() == Associativity.LEFT);
    }

    public static boolean isOperand(CharSequence token) {
        if (token == null) {
            throw new IllegalArgumentException("Argument 'token' cannot be null");
        }
        DecimalFormatSymbols currentSymbol = DecimalFormatSymbols.getInstance();
        char minusSign = currentSymbol.getMinusSign();
        if (token.equals("-") || token.equals("−") || !Character.isDigit(token.charAt(0)) && token.charAt(0) != minusSign) {
            return false;
        }
        boolean separatorFound = false;
        char decimalSeparator = currentSymbol.getDecimalSeparator();
        for (char c : token.toString().substring(1).toCharArray()) {
            if (!Character.isDigit(c)) {
                if (c == decimalSeparator && !separatorFound) {
                    separatorFound = true;
                    continue;
                }
                return false;
            }
        }
        return true;
    }

    public static boolean isOperator(CharSequence token, MathContextKeeper context) {
        return context.getOperators().stream().anyMatch(t -> t.getSymbol().equals(token));
    }

    public static boolean isToken(CharSequence token, MathContextKeeper context) {
        return isOperand(token) || isOperator(token,context) || isOpeningParentheses(token) || isClosingParentheses(token);
    }
}
