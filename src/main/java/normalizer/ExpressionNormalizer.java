package normalizer;

import util.support.MathContextKeeper;

import java.util.LinkedList;
import java.util.List;

import static util.support.TokenUtil.*;

public class ExpressionNormalizer {
    private final List<CharSequence> tokens;
    private final MathContextKeeper context;

    public ExpressionNormalizer(MathContextKeeper context) {
        this.tokens = new LinkedList<>();
        this.context = context;
    }

    public String normalize(CharSequence infix) throws IllegalArgumentException{
        tokens.clear();
        if (infix == null || infix.toString().isBlank()) {
            throw new IllegalArgumentException("Input expression cannot be null or blank");
        }
        infix = infix.toString().replaceAll(" ", "").replaceAll("\t", "");
        storeAndNormalize(infix);
        return String.join(" ", tokens);
    }

    private CharSequence storeAndNormalize(CharSequence infix) throws IllegalArgumentException {
        StringBuilder chars = new StringBuilder(infix);
        CharSequence current = null;
        CharSequence token = null;
        int endIdx = 1;
        while (endIdx <= chars.length()) {
            current = chars.substring(0, endIdx);
            if (isToken(current,context)) {
                token = current;
                if (!tokens.isEmpty() && isOperand(tokens.get(tokens.size() - 1)) && isOperator(token,context)) {
                    break;
                }
                if (!tokens.isEmpty() && isOperator(tokens.get(tokens.size() - 1),context) && isOperator(token,context)) {
                    throw new IllegalArgumentException("Duplication of operators in expression");
                }
            } else {
                if (token != null && isToken(token,context)) {
                    break;
                }
            }
            endIdx++;
        }
        if (token != null) {
            tokens.add(token);
            CharSequence result = chars.substring(token.length());
            return storeAndNormalize(result);
        } else {
            if (infix.length() > 0 && current != null) {
                throw new IllegalArgumentException("Unrecognized token '" + infix.charAt(0) + "' in infix expression");
            }
            return "";
        }
    }
}
